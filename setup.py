from setuptools import setup, find_packages

setup(
    name='custom_package',
    version='0.1',
    packages=find_packages(),
    scripts=['custom_package/custom_package_script.py'],
    entry_points={
        'console_scripts': [
            'custom_package = custom_package.__init__:start'
        ]
    },
    install_requires=[
        'paho-mqtt'
    ]
)
