import time

def adjust(message, userdata):
    """
    Adjusts the message content.

    Args:
        message (str): The original message content.

    Returns:
        str: The adjusted message content.
    """

    if "- processed message content at " not in message:
        message += " - processed message content at " + time.strftime("%Y-%m-%d %H:%M:%S")

    return message

def debug_on_message(client, userdata, message, processed_messages):
    """
    Debugs the on_message callback.

    Args:
        client (paho.mqtt.client.Client): The MQTT client.
        userdata: User data passed to the client.
        message (paho.mqtt.client.MQTTMessage): The received message.
        processed_messages (set): Set of processed messages.
    """

    original_message = message.payload.decode()
    # Check if the message has already been processed
    if original_message not in processed_messages:
        adjusted_message = adjust(original_message, userdata)
        # Publish a message
        client.publish(message.topic, adjusted_message)
        # Add the original message to the set of processed messages
        processed_messages.add(original_message)

def log_published_message(message_id, log_file):
    """
    Logs the published message ID to a file.

    Args:
        message_id: The message ID to log.
        log_file (str): The path to the log file.
    """

    print(f"Message with ID {message_id} has been logged.")
    with open(log_file, "a") as file:
        file.write(str(message_id) + "\n")

def debug_on_publish(client, userdata, mid, log_file):
    """
    Debugs the on_publish callback.

    Args:
        client (paho.mqtt.client.Client): The MQTT client.
        userdata: User data passed to the client.
        mid: The message ID of the published message.
        log_file (str): The path to the log file.
    """

    print("_______________________________")
    print("Message published successfully.")
    print("Message ID:", mid)
    print("Client Address:", client)
    print("Previous userdata", userdata)

    # flag indicating successful publication
    userdata['last_published_message_id'] = mid
    print("Last published message ID updated:", userdata['last_published_message_id'])

    # log the publication
    log_published_message(mid, log_file)

def debug_on_connect(client, userdata, flags, rc):
    """
    Debugs the on_connect callback.

    Args:
        client (paho.mqtt.client.Client): The MQTT client.
        userdata: User data passed to the client.
        flags: Flags returned by the broker.
        rc: Result code returned by the broker.
    """
    client.publish("client_id_topic", client._client_id)
    print("Connected with result code "+str(rc))
    # Print the client ID assigned by the broker
    print("Client ID: ", client._client_id)
    print("Flags: ", flags)
    print("rc: ", rc)
