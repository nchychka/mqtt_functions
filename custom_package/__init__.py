from custom_package import custom_package

# Define a set to store processed messages
log_file = "debug_ram_usage.log"
processed_messages = set()
userdata = {}


def on_message_wrapper(client, userdata, message):
    custom_package.debug_on_message(client, userdata, message, processed_messages)

def on_publish_wrapper(client, userdata, mid):
    custom_package.debug_on_publish(client, userdata, mid, log_file)

def get_ram_usage():
    mem = psutil.virtual_memory()
    used_ram = mem.used // (1024 * 1024)    # Convert to MB
    return used_ram

def publish_ram_usage(client, topic):
    while True:
        used_ram = get_ram_usage()
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
        message = f"Used RAM at {timestamp}: {used_ram} MB. "
        # Publish a message with QoS level 1
        client.publish(topic, message)
        time.sleep(5)  # Sleep for 5 seconds before publishing again

def run_mosquitto_sub(broker_address, topic):
    client = mqtt.Client(userdata=userdata)
    client.on_message = on_message_wrapper
    client.on_publish = on_publish_wrapper
    client.connect(broker_address)
    client.subscribe(topic)
    client.on_connect = custom_package.debug_on_connect

    # Start a new thread to periodically publish RAM usage
    client.loop_start()
    publish_thread = threading.Thread(target=publish_ram_usage, args=(client, topic))
    publish_thread.start()

    # Execute mosquitto_sub command
    os.system(f"mosquitto_sub -h {broker_address} -t {topic}")

def start(broker_address, topic):
    try:
        run_mosquitto_sub(broker_address, topic)
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == "__main__":
    broker_address = "localhost"
    topic = "laptop/ram_usage"
    start(broker_address, topic)
