## Custom package for debugging the performance of MQTT protocol
Generally I tried to make a code skeleton for debugging MQTT / In case somebody needs to check some underlying information about message receiving (i.e. which result code returns to the broker).

# Installation
" python3 setup.py install --user " command from a package directory (where setup.py is located locally) installs the package for a particular user (prevents from overwriting sudo dependencies/directories). 

# custom_package.py
* - contains the custom definitions of on_message, on_publish and on_connect fucntions, that allows to pass more arguments to these functions (default on_* functions are called without a possibility of adjusting arguments passed to them).

* - calling is done through the wrapper functions defined in __init__.py

# __init__.py
* - contains a toy message generator and an example of calling custom on_* functions for debugging purposes.

* - generates a real-time check of a RAM memory usage get_ram_usage() and publishes the message via publish_ram_usage(client, topic).

* - threading in run_mosquitto_sub(broker_address, topic) is not needed in case the package is used only for catching the external messages (without generating them). In case of generating messages, the code is standalone, and it can run broker subscription through this function, send messages, and call custom on_* functions for debugging purposes.

# custom_package_script.py
* - helper for running the code from the terminal i.e. ~/.local/bin/custom_package_script.py localhost laptop/ram_usage. Alternatively, the broker adress and topic are predefined in the __init__.py, if a user doesn't want to specify it in a command line.

# mqtt_ram_usage_publisher.py
* - a standalone code that generates and sends messages, that are supposed to be caught by a package. A package can catch messages generated here, or it can generate messages internally.

# issues
* - in my case the code duplicates a modified message (they write that in some cases if the loading of a message is not stable, it gets resent - thus investigating if it is my computer issue, or something else). This doesn't influence anything, just a minor remark.
