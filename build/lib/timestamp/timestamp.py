import time
import json

def add_timestamp(message):
    timestamp = time.time()
    message_with_timestamp = {
        "timestamp": timestamp,
        "message": message
    }
    return json.dumps(message_with_timestamp)
