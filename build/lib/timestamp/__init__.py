import os
import sys
import subprocess
import paho.mqtt.client as mqtt
from mqtt_timestamp import timestamp

def on_message(client, userdata, message):
    timestamped_message = timestamp.add_timestamp(message.payload.decode())
    client.publish(message.topic, timestamped_message)

def run_mosquitto_sub(broker_address, topic):
    client = mqtt.Client()
    client.on_message = on_message
    client.connect(broker_address)
    client.subscribe(topic)
    client.loop_forever()

def start(broker_address, topic):
    try:
        run_mosquitto_sub(broker_address, topic)
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == "__main__":
    broker_address = "localhost"  # Change this to your broker address
    topic = "laptop/ram_usage"  # Change this to your topic
    start(broker_address, topic)
