import os
import sys
import subprocess
import paho.mqtt.client as mqtt
from custom_package import custom_package

# Define a set to store processed messages
processed_messages = set()

def on_message(client, userdata, message):
    original_message = message.payload.decode()
    # Check if the message has already been processed
    if original_message not in processed_messages:
        print("Received message:", original_message)  # Print the received message
        print("Topic:", message.topic)  # Print the topic
        timestamped_message = custom_package.add_timestamp(original_message)
        # Check if a comment is already present in the message before appending it
        if not timestamped_message.endswith(" This message got caught by a package."):
            timestamped_message += " This message got caught by a package."  # Add a comment to the timestamped message
        print("Timestamped message:", timestamped_message)  # Print the timestamped message with a comment
        client.publish(message.topic, timestamped_message)
        # Add the original message to the set of processed messages
        processed_messages.add(original_message)

def run_mosquitto_sub(broker_address, topic):
    client = mqtt.Client()
    client.on_message = on_message
    client.connect(broker_address)
    client.subscribe(topic)
    client.loop_forever()

def start(broker_address, topic):
    try:
        run_mosquitto_sub(broker_address, topic)
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == "__main__":
    broker_address = "localhost"
    topic = "laptop/ram_usage"
    start(broker_address, topic)
