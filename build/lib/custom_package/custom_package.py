import time
import json

def add_timestamp(message):
    try:
        payload = json.loads(message)
    except json.JSONDecodeError:
        # If the message is not a valid JSON string, return it unchanged
        return message

    custom_timestamp = time.time()
    payload['timestamp'] = custom_timestamp

    return json.dumps(payload)
