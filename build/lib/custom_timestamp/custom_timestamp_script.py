#!/usr/bin/env python3

import sys
from custom_timestamp import start

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: custom_timestamp <broker_address> <topic>")
        sys.exit(1)

    broker_address = sys.argv[1]
    topic = sys.argv[2]

    start(broker_address, topic)
