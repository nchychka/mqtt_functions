import psutil
import time
import paho.mqtt.client as mqtt
import subprocess

# MQTT broker settings
broker_address = "localhost"
topic = "laptop/ram_usage"
log_file = "ram_usage.log"

def on_message(client, userdata, message):
    """
    Callback function for handling incoming messages.
    """
    # Do nothing with the received message
    pass

def get_ram_usage():
    """
    Function to retrieve RAM usage information.

    Returns:
        tuple: A tuple containing total RAM and used RAM in MB.
    """
    mem = psutil.virtual_memory()
    total_ram = mem.total // (1024 * 1024)  # Convert to MB
    used_ram = mem.used // (1024 * 1024)    # Convert to MB
    return total_ram, used_ram

def log_ram_usage(client, file_path):
    """
    Function to log RAM usage information to a file and publish it to the MQTT broker.

    Args:
        client (mqtt.Client): The MQTT client object.
        file_path (str): The file path where RAM usage information will be logged.

    Returns:
        None
    """
    try:
        total_ram, used_ram = get_ram_usage()
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S")  # Get current timestamp
        message = f"{timestamp}: Total RAM: {total_ram} MB, Used RAM: {used_ram} MB"

        # Log to file
        with open(file_path, "a") as file:
            file.write(message + "\n")

        # Publish to MQTT broker
        client.publish(topic, message)
    except Exception as e:
        print(f"Error logging RAM usage: {e}")

def main():
    """
    Main function to continuously log RAM usage to a file and publish it to the MQTT broker every 10 seconds.
    """

    # Create MQTT client
    client = mqtt.Client()
    client.connect(broker_address)
    client.on_message = on_message
    client.subscribe(topic)
    client.loop_start()

    # Run mosquitto_sub command to suppress output
    subprocess.run(["mosquitto_sub", "-h", "localhost", "-t", "laptop/ram_usage", ">", "/dev/null"])

    while True:
        log_ram_usage(client, log_file)
        time.sleep(10)

if __name__ == "__main__":
    main()



